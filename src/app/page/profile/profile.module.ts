import { CarousselComponent } from './../../feature/media/component/caroussel/caroussel.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ProfilePageRoutingModule } from './profile-routing.module';
import { ProfilePage } from './profile.page';
import { UserDetailsComponent } from 'src/app/feature/user/component/user-details/user-details.component';
import { UserListComponent } from 'src/app/feature/user/component/user-list/user-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePageRoutingModule
  ],
  declarations: [ProfilePage, UserListComponent, UserDetailsComponent]
})
export class ProfilePageModule {}
