import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-video-stream',
  templateUrl: './video-stream.page.html',
  styleUrls: ['./video-stream.page.scss'],
})
export class VideoStreamPage implements OnInit {

  constructor() { }

  videoUrl;

  name = 'Angular';
  // tslint:disable-next-line:member-ordering
  @ViewChild('videoPlayer', { static: false }) videoplayer: ElementRef;
  isPlay = false;

  ngOnInit() {
  }
  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }
  playPause() {
    // tslint:disable-next-line:prefer-const
    let myVideo: any = document.getElementById('my_video_1');
    if (myVideo.paused) { myVideo.play(); } else { myVideo.pause(); }
  }

  makeBig() {
    // tslint:disable-next-line:prefer-const
    let myVideo: any = document.getElementById('my_video_1');
    myVideo.width = 560;
  }
  makeVeryBig() {
    // tslint:disable-next-line:prefer-const
    let myVideo: any = document.getElementById('my_video_1');
    myVideo.width = 920;
    myVideo.height = 640;
  }

  makeSmall() {
    // tslint:disable-next-line:prefer-const
    let myVideo: any = document.getElementById('my_video_1');
    myVideo.width = 320;
  }

  makeNormal() {
    // tslint:disable-next-line:prefer-const
    let myVideo: any = document.getElementById('my_video_1');
    myVideo.width = 420;
  }

  skip(value) {
    // tslint:disable-next-line:prefer-const
    let video: any = document.getElementById('my_video_1');
    video.currentTime += value;
  }

  restart() {
    // tslint:disable-next-line:prefer-const
    let video: any = document.getElementById('my_video_1');
    video.currentTime = 0;
  }

}
