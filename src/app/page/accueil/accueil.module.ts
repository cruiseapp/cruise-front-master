import { CarousselAccueilComponent } from './../../feature/media/component/caroussel-accueil/caroussel-accueil.component';
import { CarousselComponent } from './../../feature/media/component/caroussel/caroussel.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccueilPageRoutingModule } from './accueil-routing.module';

import { AccueilPage } from './accueil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccueilPageRoutingModule
  ],

  declarations: [AccueilPage, CarousselComponent, CarousselAccueilComponent]

})
export class AccueilPageModule {}
