import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  loginUrl: '';
  registerUrl: '';
  token: string;


  loginUser(user: {email: string, password: string}){
    return this.http.post(this.loginUrl, user).subscribe(
      res => {
        localStorage.setItem('token', this.token);
        this.router.navigate(['/accueil']);
      })
  }

  registerUser(user: { lastName: string, firstnName: string, birthDate: number, email: string, nickName: string, password: string }){
    return this.http.post(this.registerUrl, user).subscribe(
      res => {
        localStorage.setItem('token', this.token);
        this.router.navigate(['/accueil']);
      });
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

}
