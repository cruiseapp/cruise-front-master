
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PlayButtonComponent } from './play-button/play-button.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'accueil',
    pathMatch: 'full'
  },
  {
    path: 'button',
    component: PlayButtonComponent
  },
  {
    path: 'profile',
    loadChildren: './page/profile/profile.module#ProfilePageModule'
  },
  {
    path: 'accueil',
    loadChildren: './page/accueil/accueil.module#AccueilPageModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'connexion',
    loadChildren: './page/connexion/connexion.module#ConnexionPageModule'
  },
  {
    path: 'video-stream',
    loadChildren: './page/video-stream/video-stream.module#VideoStreamPageModule'
  },
  {
    path: 'inscription',
    loadChildren: () => import('./page/inscription/inscription.module').then( m => m.InscriptionPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

