import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  starteds = [{
      name: 'Igor',
      image: '../../../../../assets/felix.png'
    },
    {
      name: 'Igor 2',
      image: '../../../../../assets/felix.png'
    }
  ];
  vieweds = [{
    name: 'Igor 3',
    image: '../../../../../assets/felix.png'
  },
  {
    name: 'Igor 4',
    image: '../../../../../assets/felix.png'
  }
];

  constructor() { }

  ngOnInit() {}

}
